---
layout: markdown_page
title: "Share Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Share Team
{: #share}

The responsibilities of this team are described by the [Share product
category](/handbook/product/categories/#dev).
