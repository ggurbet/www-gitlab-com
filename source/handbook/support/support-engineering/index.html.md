---
layout: markdown_page
title: Support Engineering Handbook
---

## Welcome to the GitLab Support Handbook
{: .no_toc}


----

### On this page
{:.no_toc}

- TOC
{:toc}

## Support Engineering At GitLab

We are on the vanguard of helping our customers, from single-instance Omnibus deployments to large 30 Node High Availability set ups. This variety means you will always be on your toes working with technologies ranging from AJAX request parsing, to Docker, Linux file permissions, Rails, and many more. Due to this extreme variablity, it's crucial that we keep our processes as lean as possible.

## Our Processes

As you work through tickets, please be aware of the following key processes:

- [How to Prioritize Tickets](/handbook/support/support-engineering/prioritizing-tickets.html)
- [How to Submit issues to Product/Development](/handbook/support/workflows/services/support_workflows/issue_escalations.html)
- [How to Submit Code to the GitLab Application](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)

The above are links to the appropriate sections of our handbook which further outline how each work.

## How We're Doing

The [Zendesk Insights dashboard](https://gitlab.Zendesk.com/agent/reporting/analytics/period:0/dashboard:buLJ3T7IiFnr) lists the activity for all of our current channels and summarizes the last 30 days (Zendesk login required).

## Our Meetings

Support has 3 meetings a week, which help us coordinate and grow together:
- APAC Support Call (Tuesdays): covers metrics, demos and open format questions.
- AMER "Ticket Crush" (Tuesdays): Demos and cooperatively solving challenging tickets as a group.
- AMER Support Call (Fridays): covers metrics and open format questions.
